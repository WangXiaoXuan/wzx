package com.wth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WthdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WthdemoApplication.class, args);
    }
}
